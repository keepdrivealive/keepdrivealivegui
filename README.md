# KeepDriveAlive

*:warning: This is a Windows application.*

This application is the Graphical User Interface (GUI) for the [KeepDriveAlive service](https://gitlab.com/keepdrivealive/keepdrivealiveservice).  
It helps to manage the service provided, including :

* Starting the service
* Stopping the service
* Restarting the service

## Overview

Ever had an external hard drive getting disconnected cause it stopped spinning while being idle?  
This is the solution then. This application will write a very small file and then edit it every 120 seconds (by default). You can change the frequency in the configuration file.  


## Installation


### Use scoop

##### Install the service and the GUI at the same time :

By adding the bucket (recommended) :
```
scoop bucket add chacas0 https://gitlab.com/ChacaS0/scoop-bucket
scoop install keepdrivealivegui
```

Or as a one-timer :
```ps
scoop install https://gitlab.com/keepdrivealive/keepdrivealivegui/-/raw/master/keepdrivealivegui.json
```

You will then be prompted to edit the configuration file. Finally, a shortcut is created so you can use it directly.

### From source

:point_right: You need to have Golang installed.
:memo: The instructions are written for bash, you can use the equivalent for Powershell or CMD
:exclamation_mark: You can also use `gow` and `sudo` for Windows

0. First create the directory for the app
	`mkdir KeepDriveAlive && cd KeepDriveAlive`
1. Clone the two projects
	```
	git clone https://gitlab.com/keepdrivealive/keepdrivealiveservice.git
	git clone https://gitlab.com/keepdrivealive/keepdrivealivegui.git
	```
2. Build the GUI
	```
	cd keepdrivealivegui
	go build -ldflags "-extldflags '-static' -H windowsgui"
	```
3. Build and install the background service
	```
	cd ../keepdrivealiveservice
	go build .
	sudo ./keepDriveAlive.exe install
	```
4. Edit the configuration file : `keepDriveAlive.exe.conf`

Now you can use `keepDriveAliveGUI.exe` located at : `KeepDriveAlive/keepdrivealivegui/keepDriveAliveGUI.exe`. 
You can also create a shortcut to this executable.

### Use the installer
*Coming soon*
