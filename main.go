// +build windows

package main

import (
	"fmt"
	"log"
	"os/exec"
	"time"

	"github.com/lxn/walk"
	. "github.com/lxn/walk/declarative"
	"golang.org/x/sys/windows"
	"golang.org/x/sys/windows/svc"
	"golang.org/x/sys/windows/svc/mgr"
)

var statusText *walk.TextEdit
var mw MainWindow

func main() {

	hasRanOnce := false

	mw = MainWindow{
		Title:   "KeepDriveAlive",
		MinSize: Size{Width: 400, Height: 450},
		Size:    Size{Width: 400, Height: 450},
		MaxSize: Size{Width: 400, Height: 450},
		Icon:    "./keepDriveAlive.ico",
		Layout:  VBox{},
		Children: []Widget{
			ImageView{
				Image:  "keepDriveAlive_transparent.png",
				Margin: 10,
			}, // ImageView
			VSplitter{
				Children: []Widget{
					PushButton{
						Text: "Start",
						OnClicked: func() {
							startService("KeepDriveAlive")
							// statusText.SetText("RUNNING")
						}, // OnClicked
					}, // PushButton
					PushButton{
						Text: "Stop",
						OnClicked: func() {
							controlService("KeepDriveAlive", svc.Stop, svc.Stopped)
							// statusText.SetText("STOPPED")
						}, // OnClicked
					}, // PushButton
					PushButton{
						Text: "Restart",
						OnClicked: func() {
							controlService("KeepDriveAlive", svc.Stop, svc.Stopped)
							time.Sleep(1500 * time.Millisecond)
							startService("KeepDriveAlive")
						}, // OnClicked
					}, // PushButton
				}, // []Widget
			}, // VSplitter
			// status display
			TextEdit{AssignTo: &statusText, ReadOnly: true},
			PushButton{
				Text: "Configure",
				OnClicked: func() {
					cmd := exec.Command("cmd", "/C start ../keepDriveAliveService/keepDriveAlive.exe.conf")
					if err := cmd.Run(); err != nil {
						log.Fatal(err)
					}
				}, // OnClicked
			}, // PushButton
		}, // []Widget
	} // MainWindow

	mw.OnMouseMove = func(x, y int, button walk.MouseButton) {
		if !hasRanOnce {
			go getServiceStatus("KeepDriveAlive", statusText)
		}
	}

	mw.Run()

}

func startService(name string) error {
	m, err := mgr.Connect()
	if err != nil {
		return err
	}
	defer m.Disconnect()

	s, err := m.OpenService(name)
	if err != nil {
		return fmt.Errorf("could not access service: %v", err)
	}
	defer s.Close()
	err = s.Start("is", "manual-started")
	if err != nil {
		return fmt.Errorf("could not start service: %v", err)
	}
	return nil
}

func controlService(name string, c svc.Cmd, to svc.State) error {
	m, err := mgr.Connect()
	if err != nil {
		return err
	}
	defer m.Disconnect()
	s, err := m.OpenService(name)
	if err != nil {
		return fmt.Errorf("could not access service: %v", err)
	}
	defer s.Close()
	status, err := s.Control(c)
	if err != nil {
		return fmt.Errorf("could not send control=%d: %v", c, err)
	}
	timeout := time.Now().Add(10 * time.Second)
	for status.State != to {
		if timeout.Before(time.Now()) {
			return fmt.Errorf("timeout waiting for service to go to state=%d", to)
		}
		time.Sleep(300 * time.Millisecond)
		status, err = s.Query()
		if err != nil {
			return fmt.Errorf("could not retrieve service status: %v", err)
		}
	}
	return nil
}

func getServiceStatus(serviceName string, statusTxt *walk.TextEdit) {
	m, err := mgr.Connect()
	if err != nil {
		log.Print("Can't get the status")
	}
	defer m.Disconnect()

	s, err := m.OpenService(serviceName)
	if err != nil {
		log.Print("Can't get the status")
	}
	defer s.Close()

	for {
		lstatus, err := s.Query()
		if err != nil {
			log.Print("Can't get the status")
		}

		switch lstatus.State {
		case svc.State(windows.SERVICE_STOPPED):
			statusTxt.SetText("STOPPED")
		case svc.State(windows.SERVICE_START_PENDING):
			statusTxt.SetText("START PENDING")
		case svc.State(windows.SERVICE_STOP_PENDING):
			statusTxt.SetText("STOP PENDING")
		case svc.State(windows.SERVICE_RUNNING):
			statusTxt.SetText("RUNNING")
		case svc.State(windows.SERVICE_CONTINUE_PENDING):
			statusTxt.SetText("CONTINUE PENDING")
		case svc.State(windows.SERVICE_PAUSE_PENDING):
			statusTxt.SetText("PAUSE PENDING")
		case svc.State(windows.SERVICE_PAUSED):
			statusTxt.SetText("PAUSED")
		}

		time.Sleep(500 * time.Millisecond)
	}
}
