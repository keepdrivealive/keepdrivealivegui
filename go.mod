module gitlab.com/keepDriveAlive/keepDriveAliveGUI

go 1.15

require (
	github.com/lxn/walk v0.0.0-20210112085537-c389da54e794
	github.com/lxn/win v0.0.0-20201111105847-2a20daff6a55 // indirect
	golang.org/x/sys v0.0.0-20210124154548-22da62e12c0c
	gopkg.in/Knetic/govaluate.v3 v3.0.0 // indirect
)
